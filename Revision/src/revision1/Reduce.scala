//package revision1
//
//// in terms of fold
//
//class MyList[T](l:List[T]) {
//  
//  // reduce
//  def myReduce(f:(T,T) => T):T = 
//    this match {
//      case Nil => throw new Error("la")
//      case h::t => (t fold h)(f)
//    }
//  
//  // my idea
//  def myReduce2(f:(T,T) => T):T = (l fold 0)(f)
//  
//  // fold
//  def myFold[U](start:U)(f:(U,T)=>U):U =
//    this match {
//     case Nil => start
//     case h::t => (tl fold fn(start, h))(fn)
//  }
//    
//    
//  
//}
//
//object TestReduce extends App {
//}
//package revision1
//
//class Filter
//
//
//class MyList[T](val l:List[T]) {
//  
//  // (val l:List[T]) my inovation
//  // Nil must be implemented
//  // hd::tl must be implemented
//   
//    def myfilter(f:T => Boolean):MyList[T] =
//       this match {
//         case Nil => this
//         case hd :: tl => 
//           if   (f(hd)) hd::tl.myfilter(f)
//           else tl.myfilter(f)
//       }
//    
//    
//}
//
//object TestFilter extends App {
//  
//  val mylist = new MyList(List(1,2,3))
//  
//  println(mylist myfilter ( x => x > 1))
//
//}
class Tea(
          val name: String = "Earl Grey", 
          val decaf: Boolean = false,
          val milk: Boolean = false,
          val sugar: Boolean = false 
          ) {


  println("I'm the constractor")
  
  def describe(): String = {
     var description = name
     
     if (decaf) description += " decaf"
     if (milk)  description += " milk"
     if (sugar) description += " sugar"
     
   description 
  }
  
  def calories(): Integer = {
     var cal = 0
     
     if (milk)  cal += 100
     if (sugar) cal += 16
   
    cal
  }
  
  println("I'm still the constractor")

}
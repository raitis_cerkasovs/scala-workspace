class Planet(val name: String, val description: String, val moons: Integer = 0) {
  
  def hasMoon(): Boolean = {
    if (moons > 0) return true
    false
  }

}
import AtomicTest._

object Driver {

  def forecast(percent: Int): String = {
    percent match {
      case 100 => "Sunny"
      case 80  => "Mostly Sunny"
      case 50  => "Partly Sunny"
      case 20  => "Mostly Sunny"
      case 0   => "Cloudy"
      case _   => "Unknown"
    }
  }

  def squareThem(args: Int*): Int = {

    var total = 0

    for (n <- args) {
      total += n * n
    }
    total
  }

  def main(args: Array[String]): Unit = {

    println()
    /**
     * Pattern Matching
     *
     */
    println("*** Pattern Matching ***")

    // 1.
    forecast(100) is "Sunny"
    forecast(80) is "Mostly Sunny"
    forecast(50) is "Partly Sunny"
    forecast(20) is "Mostly Sunny"
    forecast(0) is "Cloudy"
    forecast(15) is "Unknown"

    println()
    /**
     * Class Arguments
     *
     */
    println("*** Class Arguments ***")

    // 1. 2.
    val family1 = new Family("Mum", "Dad", "Sally", "Dick")
    family1.familySize() is 4
    val family2 = new Family("Dad", "Mom", "Harry")
    family2.familySize() is 3

    // 3.
    println("function arguments")
    squareThem(2) is 4
    squareThem(2, 4) is 20
    squareThem(1, 2, 4) is 21

    println()
    /**
     * Named and Default Arguments
     *
     */
    println("*** Named and Default Arguments ***")

    // Variable to be visible outside the class body, declare it as a var or val in the argument list.
    // 1.
    val t = new SimpleTime(hours = 5, minutes = 30)
    t.hours is 5
    t.minutes is 30

    // 2.
    val t2 = new SimpleTime(hours = 10)
    t2.hours is 10
    t2.minutes is 0

    // 3.
    val p = new Planet(name = "Mercury", description = "small and hot planet", moons = 0)
    p.hasMoon is false

    // 4.
    val earth = new Planet(moons = 1, name = "Earth", description = "a hospitable planet")
    earth.hasMoon is true

    // 5.
    val flour = new Item(name = "flour", 4)
    flour.cost(grocery = true) is 4

    val sunscreen = new Item(name = "sunscreen", 3)
    sunscreen.cost() is 3.3

    val tv = new Item(name = "television", 500)
    tv.cost(taxRate = 0.06) is 530

    println()
    /**
     * Constructors
     *
     */
    println("*** Constructors ***")

    println()
    // 1.
    val doubleHalfCaf = new Coffee(shots = 2, decaf = 1)
    val tripleHalfCaf = new Coffee(shots = 3, decaf = 2)

    doubleHalfCaf.decaf is 1
    doubleHalfCaf.caf is 1

    doubleHalfCaf.shots is 2
    tripleHalfCaf.decaf is 2

    tripleHalfCaf.caf is 1
    tripleHalfCaf.shots is 3

    // 2.
    println()
    // all constractor fields must have default values for objects to be create with different amount of arguments !!

    val tea = new Tea

    tea.describe is "Earl Grey"
    tea.calories is 0

    val lemonZinger = new Tea(decaf = true, name = "Lemon Zinger")
    lemonZinger.describe is "Lemon Zinger decaf"
    lemonZinger.calories is 0

    val sweetGreen = new Tea(name = "Jasmine Green", sugar = true)
    sweetGreen.describe is "Jasmine Green sugar"
    sweetGreen.calories is 16

    val teaLatte = new Tea(sugar = true, milk = true)
    teaLatte.describe is "Earl Grey milk sugar"
    teaLatte.calories is 116

    println()
    /**
     * Auxiliary Constructors
     *
     */
    println("*** Auxiliary Constructors ***")
    println("example from book")
    println()

    new GardenGnome(20.0, 110.0, false).
      show() is "20.0 110.0 false true"

    new GardenGnome("Bob").show() is "15.0 100.0 true true"

    println()
    println("my solution")
    new ClothesWasher("Abra cadabra")
    new ClothesWasher(32)

    println()
    /**
     * Case Classes
     *
     */
    println("*** Case Classes ***")

    // case class automatically creates all the class arguments as if you’ve put the val keyword in front of each
    // Case classes also provide a way to print objects in a nice, readable format without having to define a special display method.

    // 1.
    val per = Person("Jane", "Smile", "jane@smile.com")
    per.first is "Jane"
    per.last is "Smile"
    per.email is "jane@smile.com"

    // we don’t have to use the new keyword when creating an object.

    // 2.
    val people = Vector(
        
      Person("Jane", "Smile", "jane@smile.com"),
      Person("Ron", "House", "ron@house.com"),
      Person("Sally", "Dove", "sally@dove.com"))
      
    people(0) is "Person(Jane,Smile,jane@smile.com)"
    people(1) is "Person(Ron,House,ron@house.com)"
    people(2) is "Person(Sally,Dove,sally@dove.com)"
    
    
    
    println()
    /**
     * Parametraised types
     *
     */
    println("*** Parametraised types ***")
    println()
    
     // 1.
    def explicitDouble(d1: Double, d2: Double, d3: Double) = {     
      val vec1:Vector[Double] = Vector(d1, d2, d3)
      vec1
    }
    
    explicitDouble(1.0, 2.0, 3.0) is Vector(1.0, 2.0, 3.0)
    
    // 2.     
    def explicitList(vec2: Vector[Double]) = {
      vec2.toList
    }

    explicitList(Vector(10.0, 20.0)) is List(10.0, 20.0)
    explicitList(Vector(1, 2, 3)) is List(1.0, 2.0, 3.0)
    
    // 3.
    def explicitSet(vec3: Vector[Double]) = {
      vec3.toSet
    }

    explicitSet(Vector(10.0, 20.0, 10.0)) is Set(10.0, 20.0)
    explicitSet(Vector(1, 2, 3, 2, 3, 4)) is Set(1.0, 2.0, 3.0, 4.0)
    
      
    println()
    /**
     * Functions as Objects
     *
     */
    println("*** Functions as Objects ***")
    println()
    
    // 1.
    var str = ""
    var myStr = "1234"
    myStr.toVector
    myStr.foreach(n => str += n )   
    str is "1234"
    
    // 2.
    str = ""
    myStr.foreach(n => str += n+"," )   
    str is "1,2,3,4,"
    
    // 3.
    val dogYears = { x: Int => x * 7}
    dogYears(10) is 70 
    
    // 4.
    var s = ""
    val v = Vector(1, 5, 7, 8)
    v.foreach({ x: Int => s += x * 7 + " "})
    s is "7 35 49 56 "  
    
    // 5.
    var s2 = ""
    val numbers = Vector(1, 2, 5, 3, 7)
    numbers.foreach({ y: Int => s2 += y * y + " " })
    s2 is "1 4 25 9 49 "
    
       
    println()
    /**
     * map and reduce
     *
     */
    println("*** map and reduce ***")
    println()
    
    // 1.
    // map captures the return value from each call and stores it in a new sequence,
    val v3 = Vector(1, 2, 3, 4)
    v3.map({n: Int => n * 11 + 10}) is Vector(21, 32, 43, 54)  
    
    // 2.
    // brought an empty vector
    val v4 = Vector(1, 2, 3, 4)
    v4.foreach({n: Int => n * 11 + 10}) is Vector(21, 32, 43, 54)
    
    // 3.
    for (n <- v4) { print (n * 11 + 10 + " ")}
    
    // 6.
    println()
    val v5 = Vector(1, 2, 3)
    v5.reduce({(sum: Int, x: Int) => sum + x}) is 6
    
  }  
}
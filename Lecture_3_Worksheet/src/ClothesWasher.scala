class ClothesWasher(val modelName: String,
                    val capacity: Double) {
  
  println("start primarely constructor")
  
  def this(modelName:String){
    this(modelName, 10.5)
    println("start auxiliary constructor 1")
  }
  
  def this(capacity: Double){
    this("my name", capacity)
    println("start auxiliary constructor 2")
  }
  
  println("end primarely constructor")

}
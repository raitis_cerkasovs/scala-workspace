class Item(val name: String, val price: Double) {
  
  def cost(grocery: Boolean = false, medication: Boolean = false, taxRate: Double = 0.10): Double = {
    
    if (grocery == true || medication == true) 
      return price
   
    price + price * taxRate
  
  }

}
package question5

object Streams extends App {
  
  def iterate[T](x: T)(f: T => T): Stream[T] =
      x #:: iterate(f(x))(f)


  def iterate2[T](x: T)(f: T => T): Stream[T] =
     iterated(f) map (g => g(x))
     
  def iterated[T](f: T => T): Stream[T => T] =
     ((x: T) => x) #:: (iterated(f) map (_ andThen f))

  def iterated2[T](f: T => T): Stream[T => T] =
     iterate((x: T) => x)(_ andThen f)

}
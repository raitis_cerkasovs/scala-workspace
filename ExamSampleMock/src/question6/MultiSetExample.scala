//import math._

package question6

class MultiSetExample {
  
     type MultiSet = Int => Int
     
     def emptyMultiSet: MultiSet = { y => 0 }
     
     def singleton(x: Int): MultiSet = 
      { y =>
        if (y == x) 1 else 0
      }
     
     def union(a: MultiSet, b: MultiSet): MultiSet = 
      { y =>
        a(y) + b(y)
      }
     
//     def intersect(a: MultiSet, b: MultiSet): MultiSet = 
//      { y =>
//        min(a(y), b(y))
//      }
//     
//     def diff(a: MultiSet, b: MultiSet): MultiSet = 
//      { y =>
//        max(a(y) - b(y), 0)
//      }
     
     def primeFactors(n: Int): MultiSet = {
         def rec(i: Int, n: Int): MultiSet = {
               (i until n).find {
                n % _ == 0
                }match {
                  case None => singleton(n)
                  case Some(x) => union(singleton(x), rec(x, n / x))
                 }
         }
         rec(2, n)
     }
}
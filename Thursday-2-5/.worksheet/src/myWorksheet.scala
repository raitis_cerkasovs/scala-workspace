/* TO TAKE HOME
Recursive Algebrate Date Type Pattern
Abstraction over methods pattern
tail recursion
Lists, seq, zip, map, fold
DI google juice (reflection)
*/


/* DI  - inversion of control
decouple implementation
could be question google, spring or ...
*/


// function literals
object myworksheet {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(347); 
  println("Welcome to the Scala worksheet");$skip(164); 
  
  
  
 // square (x: Int): Int = x*x
 // square(3)
 //
  // function literal
 // f = (x: Int) -> x* y
  
  // scala has List ans sequences
  var s = List(1,2,3);System.out.println("""s  : List[Int] = """ + $show(s ));$skip(18); val res$0 = 
  s.map(x => x*x);System.out.println("""res0: List[Int] = """ + $show(res$0));$skip(68); val res$1 = 
  
  
  // cannot use if list in list
 
  // seq, zip, map
  s.head;System.out.println("""res1: Int = """ + $show(res$1));$skip(9); val res$2 = 
  s.tail;System.out.println("""res2: List[Int] = """ + $show(res$2));$skip(21); val res$3 = 
  
// tuple

(3,4,5);System.out.println("""res3: (Int, Int, Int) = """ + $show(res$3));$skip(17); 

val i = (3,4,5);System.out.println("""i  : (Int, Int, Int) = """ + $show(i ))}
  
}

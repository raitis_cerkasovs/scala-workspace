object ClassesAndObjects {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(70); 
  println("Welcome to the Scala worksheet")
  
  class Hippo {};$skip(46); 
  var hippo = new Hippo()
                                                  
  class Lion {};System.out.println("""hippo  : ClassesAndObjects.Hippo = """ + $show(hippo ));$skip(91); 
  var lion = new Lion()
  
  class Tiger {};System.out.println("""lion  : ClassesAndObjects.Lion = """ + $show(lion ));$skip(46); 
  var tiger = new Tiger()
  
  class Monkey {};System.out.println("""tiger  : ClassesAndObjects.Tiger = """ + $show(tiger ));$skip(49); 
  var monkey = new Monkey()
  
  class Giraffe {};System.out.println("""monkey  : ClassesAndObjects.Monkey = """ + $show(monkey ));$skip(47); 
  var xx = new Giraffe();System.out.println("""xx  : ClassesAndObjects.Giraffe = """ + $show(xx ));$skip(25); 
  var yy = new Giraffe();System.out.println("""yy  : ClassesAndObjects.Giraffe = """ + $show(yy ));$skip(25); 
  var cc = new Giraffe();System.out.println("""cc  : ClassesAndObjects.Giraffe = """ + $show(cc ));$skip(69); 
  
                                                  
  println(xx);$skip(14); 
  println(yy);$skip(14); 
  println(cc)}

}

class Motorboat {

  def on() = { "Motor on" }
  def off() = { "Motor off" }
  
    
  def signal(): String = {
     var flare = new Flare() 
     return flare.light
   }
}
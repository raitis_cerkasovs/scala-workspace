/**
 * @author Raitis 
 *
 */

import AtomicTest._

object Driver {
        
      def main(args: Array[String]): Unit = {
        
        val myValue = 17;        
        // 1. Cannot reassign the value.
        // myValue = 20
        
        val myValue2 = "ABC1234";    
        // 2. Cannot do it eather.
        // val myValue2 = "DEF1234";
        
        val myValue3 = 15.56;
        println(myValue3)
        
        
        /**
         * Expresions
         *
         */
         var sky = "sunny"
         var tempr = 81
         var partCloudy = false;
         
         // 1.
         if (sky == "sunny" && tempr > 80 )
           println("1 TRUE") 
           
         // 2.
         if (sky == "sunny" || partCloudy && tempr > 80 )
           println("2 TRUE")  
           
         // 3.
         if ((sky == "sunny" || partCloudy) && (tempr > 80 || tempr < 20))
           println("3 TRUE")
           
         // 4.
         var toCelsius :Double =  tempr / 32 // * 5/9 
           println(toCelsius)
         
         
         println()
        /**
         * Methods
         *
         */
         println("*** Methods ***")
         
         val methods = new Methods()
         
         // 1.
         val a = methods.getSquare(3)
         assert(a == 9, "Not Right")       
         println(a)
                  
         val b = methods.getSquare(6)
         assert(b == 36, "Not Right")       
         println(b)
                  
         val c = methods.getSquare(5)
         assert(c == 25, "Not Right")       
         println(c)
         
         // 2.
         val t1 = methods.isArg1GreaterThanArg2(4.1, 4.2)
         assert(t1 == false, "Not Right")
         println(t1)
         
         val t2 = methods.isArg1GreaterThanArg2(2.1, 1.2)
         assert(t2 == true, "Not Right")
         println(t2)
         
         // 3.
         val m1 = methods.manyTimesString("abc", 3)
         assert("abcabcabc" == m1, "Not Right")
         println(m1)
         
         val m2 = methods.manyTimesString("123", 2)
         assert("123123" == m2, "Not Right")
         println(m2)
         
        
         println()
        /**
         * Classes and Objects
         *
         */
         println("*** Classes and Objects ***")
         
         var s1 = "Sally"
         var s2 = "Sally"
         
         if (s1.equals(s2)) println("equals")
         if (s1 == s2) println("equals")
         
         // 1.
         val sailboat = new Sailboat
         
         val r1 = sailboat.raise()
         assert(r1 == "Sails raised", "Expected Sails raised, Got " + r1)

         val r2 = sailboat.lower()
         assert(r2 == "Sails lowered", "Expected Sails lowered, Got " + r2)
         
         val motorboat = new Motorboat
         
         val mot1 = motorboat.on()
         assert(mot1 == "Motor on", "Expected Motor on, Got " + mot1)

         val mot2 = motorboat.off()
         assert(mot2 == "Motor off", "Expected Motor off, Got " + mot2)
         
         // 2.
         val flare = new Flare
         val f1 = flare.light
         assert(f1 == "Flare used!", "Expected Flare used!, Got " + f1)
         
         // 3.
         val sailboat2 = new Sailboat
         val signal = sailboat2.signal()
         assert(signal == "Flare used!", "Expected Flare used! Got " + signal)
         
         val motorboat2 = new Motorboat
         val flare2 = motorboat2.signal()
         assert(flare2 == "Flare used!", "Expected Flare used!, Got " + flare2)
         
         
         println()
        /**
         * Classes and Objects
         *
         */
         println("*** Fields ***")
         
         val cup = new Cup
         
         cup.add(45) is 45
         cup.add(-55) is 0
         cup.add(10) is 10
         cup.add(-9) is 1
         cup.add(-2) is 0
         
         println("change, because  it is public")
         cup.percentFull = 56
         cup.percentFull is 56
         
         println("set, get")
         cup.set(56)
         cup.get() is 56
         
        
         println()
        /**
         * Vectors
         *
         */
         println("*** Vectors ***")
         
         // 1. 2.
         val v1 = Vector(1,2,3,4,5)
         val vd = Vector(1.1, 2.1, 3.1, 4.1, 5.1)
         val v2 = Vector("one", "two", "three", "for", "five")
         val vc = Vector(v1, v2)
         println(vc)
         
         // 3.
         val v = Vector("The", "dog", "visited", "the", "fire", "station") 
         var sentence = ""
         for (result <- v) sentence += result + " "
         sentence.toString() is "The dog visited the fire station "
         
         // 4.
         println(v1.sum)
         println(v1.min)
         println(v1.max) 
         
         println()
         println(vd.sum)
         println(vd.min)
         println(vd.max)
         
         // 5.
         val myVector1 = Vector(1,2,3,4,5,6)
         val myVector2 = Vector(1,2,3,4,5,6)
         
         (myVector1 == myVector2) is true
         
    }
}

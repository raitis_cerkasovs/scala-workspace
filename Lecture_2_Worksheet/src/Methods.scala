class Methods {
  
  // In Scala methods are declared by writing “def methodName(param1: param1Type, param2: param2Type) : returnType = { method body }”.
  // The compiler can infer the return type from what is being returned. We can however remove the return keyword. The compiler will just assume that we want to return the value of the last statement.
  //the dot after the variable name and the parenthesis after the method name is optional  when calling methods that have either none or only a single parameter/
  /*original.*(3)
    original *(3)
    original * 3 
  */
  //Call parameters explicitly
  //println(helper.ellipse("Hello world!", 10))
  //println(helper.ellipse("Hello world!", maxLength = 10))
  
  def getSquare(myValue: Int):Int = {
    myValue * myValue
  }
  
  def isArg1GreaterThanArg2(arg1: Double, arg2: Double): Boolean = {
    if (arg1 > arg2)
      return true
    else
      return false    
  } 
  
  def manyTimesString(myString: String, times: Int): String = {
    
    var result = ""
    
    for ( i <- 1 to times) {
      result = result + myString
    }
    
    result
  }
    
}
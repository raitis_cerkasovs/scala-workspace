class Sailboat {

  def raise() = { "Sails raised" }
  def lower() = { "Sails lowered" }
  
  def signal(): String = {
     var flare = new Flare() 
     return flare.light
   }
  
}
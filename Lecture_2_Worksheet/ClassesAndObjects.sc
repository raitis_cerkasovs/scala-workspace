object ClassesAndObjects {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  class Hippo {}
  var hippo = new Hippo()                         //> hippo  : ClassesAndObjects.Hippo = ClassesAndObjects$$anonfun$main$1$Hippo$1
                                                  //| @11baa65
                                                  
  class Lion {}
  var lion = new Lion()                           //> lion  : ClassesAndObjects.Lion = ClassesAndObjects$$anonfun$main$1$Lion$1@f4
                                                  //| 38e
  
  class Tiger {}
  var tiger = new Tiger()                         //> tiger  : ClassesAndObjects.Tiger = ClassesAndObjects$$anonfun$main$1$Tiger$1
                                                  //| @c7da1e
  
  class Monkey {}
  var monkey = new Monkey()                       //> monkey  : ClassesAndObjects.Monkey = ClassesAndObjects$$anonfun$main$1$Monke
                                                  //| y$1@1464ce8
  
  class Giraffe {}
  var xx = new Giraffe()                          //> xx  : ClassesAndObjects.Giraffe = ClassesAndObjects$$anonfun$main$1$Giraffe$
                                                  //| 1@1829d67
  var yy = new Giraffe()                          //> yy  : ClassesAndObjects.Giraffe = ClassesAndObjects$$anonfun$main$1$Giraffe$
                                                  //| 1@1dfb72a
  var cc = new Giraffe()                          //> cc  : ClassesAndObjects.Giraffe = ClassesAndObjects$$anonfun$main$1$Giraffe$
                                                  //| 1@1c68925
  
                                                  
  println(xx)                                     //> ClassesAndObjects$$anonfun$main$1$Giraffe$1@1829d67
  println(yy)                                     //> ClassesAndObjects$$anonfun$main$1$Giraffe$1@1dfb72a
  println(cc)                                     //> ClassesAndObjects$$anonfun$main$1$Giraffe$1@1c68925

}
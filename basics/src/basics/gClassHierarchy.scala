package basics

// Only Abstract class can hold functions without Implementation
// Abstract class cannot be instantiated
// Persistant data structures - insert update are not ruin old structure, because old structure is kept. 
// If function is Implemented then in subclas "override" must be added

// Trait - abstract class. Traits resamble interfaces, but are more powerful because has a default implementation. 
//Never can have parameters.
// Scala - single inheritance language.
// Using in this way:
// Calass extends Superclass with Trait with Trait2 ... 

// Type parametraisation - classes or methods can have types as parameters.

// Head-Tail lists - fundamental structure in functional languages, imutable linked list

trait IntSet {
  def incl(x: Int): IntSet
  def contains(x: Int): Boolean
  def union(other: IntSet): IntSet
  def intersection(other: IntSet): IntSet
  def excl(x: Int): IntSet
  def isEmpty: Boolean
}

case class EmptySet() extends IntSet {
  override def contains(x: Int): Boolean = false
  override def incl(x: Int): IntSet = new NonEmptySet(x, new EmptySet, new EmptySet)
  override def union(other: IntSet): IntSet = other
  override def intersection(other: IntSet): IntSet = this
  override def excl(x: Int): IntSet = this
  override def isEmpty: Boolean = true
}

case class NonEmptySet(elem: Int, val left: IntSet, val right: IntSet) extends IntSet {
    
  override def contains(x: Int): Boolean =
    if (x < elem) left contains x
    else if (x > elem) right contains x
    // if not less and not biger it means equal and we found it
    else true
    
  override def incl(x: Int): NonEmptySet =
    // new element is greated:
    // Persistant data structures - insert update are not ruin old structure, because old structure is kept. 
    if (x < elem) new NonEmptySet(elem, left incl x, right)
    else if (x > elem) new NonEmptySet(elem, left, right incl x)
    // else element already exist
    else this
    
  override def union(other: IntSet): IntSet =
    left.union(right).union(other).incl(elem)
    
  override def intersection(other: IntSet): IntSet = {
      val l = left.intersection(other)
      val r = right.intersection(other)
      val s = l.union(r)
      if (other.contains(elem)) s.incl(elem) else s
    }
  
  override def excl(x: Int): IntSet = 
      if (x < elem) new NonEmptySet(elem, left.excl(x), right)
      else if (x > elem) new NonEmptySet(elem, left, right.excl(x))
      else left.excl(x)
    
  override def isEmpty: Boolean = false
}

object TestSet extends App {
  
  val tsStart = new NonEmptySet(5, new EmptySet, new EmptySet)
 // val ts1 = tsStart.incl(7).incl(3) // or:
  val ts1: NonEmptySet = tsStart incl 7 incl 3 incl 8 // 5,7,3
  val ts2 = tsStart.incl(3) // 5,3
  
  
  println( "ts1 left: " +
      ts1.left   
  )  
  println( "ts1 right: " +
      ts1.right
  ) 
  
  println()
  
  println( "ts2 left: " +
      ts2.left   
  )  
  println( "ts2 right: " +
      ts2.right
  ) 
  
  println()

  println(ts1 union ts2) 
  
  println()

  println(ts1 intersection ts2)

//  println("ts1 (5, 7, 3)")
//  println(ts1)
//  
//  println("ts1 contain 7 (true)")
//  println(ts1.contains(7))
//  
//  println("ts2 (5, 3)")
//  println(ts2)
//  
//  println("ts2 contain 7 (false)")
//  println(ts2.contains(7))
//
//  println("union (5, 3, 7)")
//  // println(ts1.union(ts2)) or:
//  println(ts1 union ts2)
//  
//  println("intersection (5, 3)")
//  println(ts1 intersection ts2)
//  
//  println("exclude (ts1 5, 3))")
//  println(ts1.excl(7))
//  
//  println("isEmpty false")
//  println(ts1.isEmpty)
//  
//  println(tsStart.excl(5))
//  println("Set now is empty, true!!!! Have to be solved")
//  println(tsStart.isEmpty)
  }
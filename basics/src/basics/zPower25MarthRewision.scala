package basics

// Write a Scala program that reads a double value b and an integer value k as input, and them computes the value b raised to the power k and prints it. If the value of k is less than or equal to zero then that is considered an error.
//Requirements:  You must write a recursive function to compute the power  Your solution must be tail-recursive


object zPower25MarthRewision extends App {
      
  def raise(b: Double, k: Int): Double = {  
    
    def helper(value: Double, b:Double, k: Int): Double =
      k match {
        case 1 =>  value
        case _ =>  helper(value*b, b, k-1)
      }
    
    if (k <= 0) 
      {println("Wrong parameter " + k); 0 }     
    else
      helper(b, b, k)   
  }
  

///////////////////////////////////////// Keith
  
   def raiseToPower(base: Double, power: Int): Option[Double] = {
    def raiseToPowerWork(base: Double, power: Int, accum: Double): Option[Double] = {
      if (power == 0)
        Some(accum)
      else
        raiseToPowerWork(base, power - 1, accum * base)
    }

    if (power <= 0)
      None
    else
      raiseToPowerWork(base, power, 1.0)
  }
   
/////////////////////////////////////////////////////////
 
  val b = readLine().toDouble
  val k = readLine().toInt
  
  
  println(raiseToPower(b, k).getOrElse(s"Invalid value $k")) 
  println(raise(b, k))
}
package basics

object iPatternMatching extends App {
  
  abstract class IntTree
  
  case object EmptyTree extends IntTree
  case class Node(elem: Int, left: IntTree, right: IntTree) extends IntTree
  
  def contains(tree: IntTree, value: Int): Boolean = tree match { 
    case EmptyTree => false
    case Node(e, l, r) =>
      if (value < e) contains(l, value)
      else if (value > e) contains(r, value) 
      else true
  }

  
  def insert(t: IntTree, v: Int): IntTree = t match {
    case EmptyTree => Node(v, EmptyTree, EmptyTree)
    case Node(e, l, r) => 
      if (v < e) Node(e, insert(l, v), r)
      else if (v > e) Node(e, l, insert(r, v))
      else t
  }
  
  //////////////////////////////////// TEST
     
  def containsTest(tree: IntTree, value: Int): Boolean = tree match { 
    case EmptyTree => { println("stop");  false}
    case Node(e, l, r) =>
      if (value < e) { println("x"); contains(l, value) }
      else if (value > e) { println("y"+e+r); contains(r, value) }
      else { println("true"); true }
  }
    
  def insertTest(tree: IntTree, value: Int): IntTree = tree match {
    case EmptyTree =>{  println("empty"); Node(value, EmptyTree, EmptyTree) }
    case Node(e, l, r) => 
      if (value < e) { println("left"); Node(e, insert(l, value), r) }
      else if (value > e) { println("right"); Node(e, l, insert(r, value)) }
      else { println("correct"); tree }
  }
  
  //////////////////////////////////// IMPLEMENTATION

  
  val tree = insert(insert(insert(EmptyTree, 10), 1), 20)
  
//  println(containsTest(tree, 15))
//  println(insertTest(tree, 15))

  
  println(tree)
  println(contains(tree, 20))
  println(contains(tree, 15))
  val tree2 = insert(tree, 75) 
  println(tree2)
  println(contains(tree2, 75))
 

  }
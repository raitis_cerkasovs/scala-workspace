// Type parameterization - classes or methods can have types as parameters.
// Head-Tail lists - fundamental structure in functional languages, immutable linked list
// Type Parameters do not affect evaluation (type erasure) They are meant for compiler  but not for runtime
// Polymorphism:
//  1) subtyping - instances of subclass can be passed to base class
//  2) generics - instances are created by Type Parameterization

package basics

trait List[T] {
  def isEmpty: Boolean
  def head: T
  def tail: List[T]
}

class Cons[T](val head: T, val tail: List[T]) extends List[T] {
  def isEmpty = false
}

class Nil[T] extends List[T] {
  def isEmpty = true
  def head: Nothing = throw new NoSuchElementException("nill head")
  def tail: Nothing = throw new NoSuchElementException("nill tail")
}


object zHeadTailTypeParameter extends App {
  
  def singleton[T](elem: T) = new Cons[T](elem, new Nil[T])
    
  println(singleton[Int](1))
  // or:
  println(singleton(1))
  println(singleton[Boolean](true))
  
  def nth[T](n: Int, list: List[T]): T = {
    
    if (list.isEmpty) throw new IndexOutOfBoundsException("List Is Empty")
    else if (n == 0) list.head  
    else nth(n - 1, list.tail)
  }
  
  val myList = new Cons(1, new Cons(2, new Cons(3, new Nil)))
  
  println(nth(2, myList))
  println(nth(-1, myList))
  
  }
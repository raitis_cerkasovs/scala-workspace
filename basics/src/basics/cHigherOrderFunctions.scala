// Higher order functions - functions what take functions as argument or return a function
// Lower order - opposite

// (x: Int) => x*x*x - Anonomous function

package basics

object cHigherOrderFunctions extends App {

  def sumInts(a: Int, b: Int): Int =
    if (a > b) 0
    else a + sumInts(a + 1, b)

    
  //////////////////////// my later test

  def sumLLA(f: Int => Int)(a: Int, b: Int): Int = {
      
    def iter(a: Int, result: Int): Int = {
      if (a == b) result
      //else iter(a + 1, result + a) or:
      else iter(a + 1, result + f(a))
    }
    iter(a, b)
  } 
  
  println("Sum LAA: " + sumLLA(x => x)( 3, 6))
  
    //////////////////////// 
  

  def sum(f: Int => Int, a: Int, b: Int): Int =
    if (a > b) 0
    else f(a) + sum(f, a + 1, b)

  def sumInts2(a: Int, b: Int) = sum(x => x, a, b)

  ////////

  def sumR(f: Int => Int, a: Int, b: Int) = {

    def iter(a: Int, result: Int): Int = {
      if (a > b) result
      else iter(a + 1, f(a) + result)
    }
    iter(a, 0)
  }

  println(sumInts(3, 5))
  println(sumInts2(3, 5))

  println(sumR(x => x, 3, 5))
  // or for clearance
  println(sumR(x => x * x, 3, 5))

}
/*
17. Define the following function in terms of for:
      def flatten[A](xss: List[List[A]]): List[A] =
          (xss :\ (Nil: List[A])) ((xs, ys) => xs ::: ys)
*/

object sFlattern extends App {
  
 def flatten[A](xss: List[List[A]]): List[A] =
     (xss :\ (Nil: List[A])) ((xs, ys) => xs ::: ys)
     
 def flattenFor[B](xss: List[List[B]]): List[B] =
     for(xs <- xss; x <- xs) yield x

val xss = List(List(1,2),List(3,4))

println("flatten: " + flatten(xss))
println("flatten in terms of for: " + flattenFor(xss) + "\n")     
     

     
 def testTwoRanges = {     
     var a = 0; 
     var b = 0;
     
     for (a<-1 to 2; b <- 1 to 2) {
         println("a: " + a)
         println("b: " + b)
        } 
}
 
testTwoRanges;
     
def testYield(l:List[Int]) = {   
     for (i <- l if i>3) yield i       
}    

println("\nYield" + testYield(List(2,3,4,5))) 
}
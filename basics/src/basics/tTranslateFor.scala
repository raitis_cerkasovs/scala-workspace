/*
   18. Translate
      for (b <- books; a <- b.authors if a startsWith "Bird") yield b.title
      for (b <- books if (b.title indexOf "Program") >= 0) yield b.title
   to higher-order functions.
*/

object tTranslateFor extends App {
  
  case class Book(title: String, authors: List[String])

      val books: List[Book] = List(
         Book("Structure and Interpretation of Computer Programs",
         List("Abelson, Harold", "Sussman, Gerald J.")),
         Book("Principles of Compiler Design",
         List("Aho, Alfred", "Ullman, Jeffrey")),
         Book("Programming in Modula2",
         List("Wirth, Niklaus")),
         Book("Introduction to Functional Programming",
         List("Bird, Richard")),
         Book("The Java Language Specification",
         List("Gosling, James", "Joy, Bill", "Steele, Guy", "Bracha, Gilad")))
         
         
         
         
         for (b <- books; a <- b.authors if a startsWith "Bird") yield println(b.title)
         
         println(
         books.flatMap( x => x.authors.filter ( y => y startsWith "Bird" ).map( z => x.title ))
         )
         
         // or
         println(
         books.flatMap { x => x.authors.filter { y => y.startsWith("Bird") }.map { z => x.title }}
         )
  
  println("//////////////////////////////////////////////////////////////////////////////////////")
  
         for (b <- books if (b.title indexOf "Program") >= 0) yield println(b.title)
         
         println(
         books.filter { x => (x.title.indexOf("Program")) >= 0 }.map { x => x.title }
         )
         
         // or        
         println("\nMy:" + books.filter { x => x.title.indexOf("Program")  >= 0}.map { x => x.title })
         
    
         
         
         


  
  
  
  

}
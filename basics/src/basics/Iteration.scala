package basics

// merge functions are important

// to dificult for exam

object Iteration extends App {
  
  def iterate[T] (x: T)(f: T => T): Stream[T] = {
    x #:: iterate(f(x))(f)
  }
// # - treated as a list
  
  def iterated[T](f: T => T): Stream[T => T] = {
    ((x: T) => x) #:: (iterated(f) map (_ andThen f))
 // dificult version   iterate((x: T) => x)(_ andThen f)
  }
}
  
/*
16. Given a chess board of size n, place n queens on it so that no queen is in check with
    another. Consider the following code:
       
        def queens(n: Int): List[List[Int]] = {
          
          def placeQueens(k: Int): List[List[Int]] =
              if (k == 0) List(List())
              else for {queens <- placeQueens(k - 1)
                 column <- List.range(1, n + 1)
              if isSafe(column, queens, 1)} yield column :: queens
              
          placeQueens(n)
         }

    Write the function
        def isSafe(col: Int, queens: List[Int], delta: Int): Boolean
    which tests whether a queen in the given column col is safe with respect to the
    queens already placed. Here, delta is the difference between the row of the queen to
    be placed and the row of the first queen in the list.
 */

object rQueens extends App {
  
  def queens(n: Int): List[List[Int]] = {
   
    def placeQueens(k: Int): List[List[Int]] =     
      if (k == 0) List(List())
      else for {       
        queens <- placeQueens(k - 1)
        column <- List.range(1, n + 1)
        if isSafe(column, queens, 1)      
      } yield column :: queens
      
    placeQueens(n)
  }
  
  def isSafe(col: Int, queens: List[Int], delta: Int): Boolean = 
   queens match {
     case Nil => true
     case x :: xs =>  ( col != x 
                     && col != x-delta
                     && col != x+delta 
                     && isSafe(col, xs, delta +1))
    }
 
  println(queens(4))
  println(queens(4).length)
  
}
package basics

// Functions is abrivation of class and is an object
// anonymous function is actually anonomous class:
// (x: Int) => x * x would be
//   { class AnonFun extends
//     or:
//     new Function1[Int, Int] {
//         def applay(x: Int) = x * x
//         }
//      new AnonFun
//    }


//////////////////////////////////////////////////////////////
// GIVEN
abstract class Nat {
  def isZero: Boolean
  def predecessor: Nat
  def successor: Nat
  def +(that: Nat): Nat
  def -(that: Nat): Nat
}

// INTEGER
abstract class Integer(n: Int) {
  def isZero: Boolean
  def predecessor: Integer
  def successor: Integer
  def + (that: Integer): Integer
  def - (that: Integer): Integer
  def negate: Integer
  def isPositive: Boolean
}



//////////////////////////////////////////////////////////////
// GIVEN
object Zero extends Nat {
  override def isZero: Boolean = true
  override def predecessor: Nat = sys.error("negative number")
  override def successor: Nat = new Succ(Zero)
  override def +(that: Nat): Nat = that
  override def -(that: Nat): Nat = if (that.isZero) Zero
  else sys.error("negative number")
}

// INTEGER
object IntZero extends Integer(0) {
  def isZero: Boolean = true
  def isPositive: Boolean = false
  def predecessor: Integer = new NegInt(this, -1)
  def successor: Integer = new PosInt(this, 1)
  
  def + (that: Integer): Integer = that
  def - (that: Integer): Integer = {
    def iter(n: Integer, p: Integer): Integer = {
      if (n.isZero) p
      else {
        val nextN = if (n.isPositive) n.predecessor else n.successor
        val nextP = if (n.isPositive) p.predecessor else p.successor
        iter(nextN, nextP)
      }
    }
    iter(that, this)
  }
  def negate: Integer = this
}

//////////////////////////////////////////////////////////////
// GIVEN - SUCCESSOR
class Succ(x: Nat) extends Nat {
  override def isZero: Boolean = false
  override def predecessor: Nat = x
  override def successor: Nat = new Succ(this)
  override def +(that: Nat): Nat = x + that.successor
  override def -(that: Nat): Nat = if (that.isZero) this
  else x - that.predecessor
}

//////////////////////////////////////////////////////////////
// NEGATIVE INTEGER
class NegInt(last: Integer, n: Int) extends Integer(n: Int) {
  def isZero: Boolean = false
  def isPositive: Boolean = false
  def predecessor: Integer = new NegInt(this, n-1)
  def successor: Integer = last
  def +(that: Integer): Integer = last + that.predecessor
  def -(that: Integer): Integer = last - that.successor
  def negate: Integer = IntZero - this
}




// POSITIVE INTEGER

class PosInt(last: Integer, n: Int) extends Integer(n: Int) {
  def isZero: Boolean = false
  def isPositive: Boolean = true
  def predecessor: Integer = last
  def successor: Integer = new PosInt(this, n+1)
  def +(that: Integer): Integer = last + that.successor
  def -(that: Integer): Integer = last - that.predecessor
  def negate: Integer = IntZero - this
}



//def isPositive: Boolean
//def negate: Integer

object hFunctionAsObject extends App {

  println("hallo")

}


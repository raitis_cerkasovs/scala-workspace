package basics

object aOne extends App{
     
     def abs(x: Double) = if   (x > 0) x 
                          else -x
     
     def square(x: Double) = x * x
     
     //
     
     def improve(guess: Double, x: Double) =
         (guess + x / guess) / 2
         
     def isGoodEnough(guess: Double, x: Double): Boolean =
         // abs(square(guess) - x) < 0.001 
         // answer:
         abs(square(guess) - x) / x < 0.001 

     // Two options ^
             
     def sqrtIter(guess: Double, x: Double): Double =
         if (isGoodEnough(guess, x)) guess
         else sqrtIter(improve(guess, x), x)

     // start with 1 ^     
         
     def sqrt(x: Double) = sqrtIter(1.0, x)
 
     
   // println(sqrtIter(-10, 121))    
   //  println(sqrt(23))
     println(sqrt(1e-6))
   //  println(sqrt(1e60))
     
     
   // Take coefficient instead of value (.. / x). 
   // For small numbers any result can be smaller than 0.001 so any result will do and therefore can't be precise. 
   // Large doubles can use space in memory where there is no space for 3 positions after comma.
   // Therefore distance between two numbers never going to be less than 0.001
}

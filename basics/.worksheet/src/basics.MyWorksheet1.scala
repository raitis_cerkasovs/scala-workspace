package basics

object MyWorksheet1 {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(81); 
  println("Welcome to the Scala worksheet");$skip(82); 
  
  def factorial(n: Int): Int =
    if (n == 0) 1
    else n * factorial(n - 1);System.out.println("""factorial: (n: Int)Int""");$skip(20); val res$0 = 
    
  factorial(3);System.out.println("""res0: Int = """ + $show(res$0));$skip(182); 
  
  def factorialTail(n: Int): Int ={
    
       def helper(n: Int, count: Int) =
           if (n == 0) 1
           else helper
     
     println("x")
     helper(n, count)
  };System.out.println("""factorialTail: (n: Int)Int""")}
  
}

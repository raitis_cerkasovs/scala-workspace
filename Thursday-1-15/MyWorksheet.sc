// ment to test things out
// Any - reference or value

object MyWorksheet {


  println("Welcome to the my worksheet")          //> Welcome to the my worksheet
  println("Something nelse")                      //> Something nelse
  
  if (1 < 2) "hallo"                              //> res0: Any = hallo
  
  
  if (1 < 2) "left"                               //> res1: Any = left
  
  println("something + " + "something")           //> something + something
  
  println("halllo".toUpperCase())                 //> HALLLO
  
  println("halllo".take(3))                       //> hal
   
  println("halllo" take 2 )                       //> ha
  
  123.toByte                                      //> res2: Byte = 123
  
  "day in the zoo is wondedrful" split " "        //> res3: Array[String] = Array(day, in, the, zoo, is, wondedrful)
  
  def name: String = "blabla"                     //> name: => String
  
 // object Test4 {def Hallo (name:String)}
 //object Test5 { val name = "efref"
 //               def halloother:String} = name +"wsefwe"
 
  {
 println("side efect")
 println("side efect 2")
 println("side efect 3")
 }                                                //> side efect
                                                  //| side efect 2
                                                  //| side efect 3
 
 
 assert(square(2.0) == 4.0)                       //> scala.NotImplementedError: an implementation is missing
                                                  //| 	at scala.Predef$.$qmark$qmark$qmark(Predef.scala:225)
                                                  //| 	at MyWorksheet$$anonfun$main$1.square$1(MyWorksheet.scala:44)
                                                  //| 	at MyWorksheet$$anonfun$main$1.apply$mcV$sp(MyWorksheet.scala:40)
                                                  //| 	at org.scalaide.worksheet.runtime.library.WorksheetSupport$$anonfun$$exe
                                                  //| cute$1.apply$mcV$sp(WorksheetSupport.scala:76)
                                                  //| 	at org.scalaide.worksheet.runtime.library.WorksheetSupport$.redirected(W
                                                  //| orksheetSupport.scala:65)
                                                  //| 	at org.scalaide.worksheet.runtime.library.WorksheetSupport$.$execute(Wor
                                                  //| ksheetSupport.scala:75)
                                                  //| 	at MyWorksheet$.main(MyWorksheet.scala:4)
                                                  //| 	at MyWorksheet.main(MyWorksheet.scala)
 assert(square(2) == 4)
  
  
 def square(Int: Double):Double = ???
  




















   
  
   
}
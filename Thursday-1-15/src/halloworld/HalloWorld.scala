package halloworld

object HalloWorld {
  
  // Unit - similar to void, return type
  // obbject is a singleton class
  // [String] generics
   
  def main(args: Array[String]): Unit = {
    println("Hallo")
  }

}
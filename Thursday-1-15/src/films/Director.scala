package films

class Director(val name: String,
               val surname: String, 
               val yearBirth: Integer) {
  
  def fullName : String = name + " " + surname
    

    def copy(
      firstName: String = this.name,
      lastName: String = this.surname,
      yearOfBirth: Int = this.yearBirth) = new Director(firstName, lastName, yearOfBirth)
      

}
  

 
  
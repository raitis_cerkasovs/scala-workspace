package films

class Film (val name: String, 
            val yearOfReleace: Int, 
            val imbRating: Double,
            val director: Director
              ){
  
def directorAge: Integer = yearOfReleace - director.yearBirth

def isDirectedBy(director:Director) = this.director == director

def copy(name: String = this.name, 
         director: Director = this.director, 
         yearOfReleace: Integer = this.yearOfReleace, 
         imdRating: Double = this.imbRating) = new Film (name, yearOfReleace, imdRating, director)

}
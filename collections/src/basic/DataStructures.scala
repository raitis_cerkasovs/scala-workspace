package basic

object DataStructures extends App {
  
  // List
  val mylist:List[Int] = List(1,2,3) 
  println("list: " + mylist)
  
  // Set
  val myset:scala.collection.immutable.Set[Int] = Set(1,2,1)
  println("Set, has no dublicates: " + myset)
  
  // Tuple
  val mytuple:(String, Int) = ("localhost", 80)
  println("groups items, unlake case classes has no name" + mytuple)
  println("tuples has accesors by position: " + mytuple._1 + " and " + mytuple._2)
  // can be used with pattern matching:
  mytuple match {
     case ("localhost", port) => println("matching localhost")
     case (host, port) => println("nothing to match")
  // by arrow
  val mytuple2:(Int, Int) = 1 -> 2
  println(mytuple2)
  
  }
}
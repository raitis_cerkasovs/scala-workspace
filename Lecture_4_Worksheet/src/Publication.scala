sealed trait Publication 

sealed trait Manuscript { 
 val author: Author
 val length: Int
}

final abstract class Book(author: Author) extends Publication with Manuscript 

final abstract class Issue {
  val manuscripts: Seq[Manuscript]  
  val number: Int 
  val volume: String
}

final abstract class Periodical(editor: Editor, issues: Seq[Issue]) extends Publication

final class Author
final class Editor







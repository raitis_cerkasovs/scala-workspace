
// Trait cannot be instantiated
// Diferent from abstract classes with cannot have constructor arguments

// saled - may not be directly inherited (except if it is in same file)
sealed trait Shape {
  
  def sides(): Double   
  def perimeter(): Double 
  def area(): Double  
}


sealed trait Color {
  def R: Int = 0
  def G: Int = 0
  def B: Int = 0
  
  val red = "red"
  val yellow = "yellow" 
  val pink = "pink"
  
  def meansColor(r: Int, g: Int, b: Int): String = { " " + r + " " +  g + " " + b}
  
  def meansLight(s: String): String = { s }
}

abstract class Rectangular

// by comenting out case class is unlocated in the Draw object
final case class Rectangle(val side1: Double, side2: Double) extends Rectangular with Shape with Color {
  
  override def sides() = {
    println("sides from Rectangle")
    val sidesNr = 4
    sidesNr
  }
  
  override def perimeter() = {
    println("perimeter from Rectangle")
    val perimeter = side1 * 2 + side2 * 2
    perimeter
  }
  
  override def area() = {
    println("area from Rectangle")
    val area = side1 * side2
    area
  }
}





final case class Square(val side: Double) extends Rectangular with Shape {
  
  override def sides() = {
    println("sides from Square")
    val sidesNr = 4
    sidesNr
  }
  
  override def perimeter() = {
    println("perimeter from Square")
    val perimeter = side * 4
    perimeter
  }
  
  override def area() = {
    println("area from Square")
    val area = side * side
    area
  }
}






final case class Circle(val radius: Double) extends Shape {
  
  println("This is circle with radius " + radius)
  
  override def sides() = {
    println("sides from Circle")
    val sidesNr = 0
    sidesNr
  }
  
  override def perimeter() = {
    println("perimeter from Circle")
    val perimeter = 2 * math.Pi * radius
    perimeter
  }
  
  override def area() = {
    println("area from Circle")
    val area = math.Pi * radius * radius
    area
  }
  
//  override def description():String = {
//    "A circle of radius " + radius
//  }

}









object Draw extends App { 
  
  
  
  
  
  def apply(shape: Shape) = {   
    println(shape.toString())
  }
  
  println("start")
  
  Draw(Circle(10))
  Draw(Rectangle(3,4))
  println(Rectangle(3,4).meansColor(1, 2, 3))
  println(Rectangle(3,4).yellow)
  println(Rectangle(3,4).G)
  
  println("end")
  
}
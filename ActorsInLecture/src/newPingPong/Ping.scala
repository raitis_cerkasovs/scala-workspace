//package newPingPong
//
//// import akka.actor.Actor
//
//case object PingMessage
//case object PongMessage
//case object StartMessage
//case object StopMessage
//
//abstract class Ping(pong: ActorRef) extends Actor { 
//  
//  var count = 0
//  
//  def incAndPrint: Unit = {
//    count += 1
//    println("Ping")
//  }
//  
//  override def recieve = {
//    case StartMessage =>
//      pong ! pingMessage
//    case PongMessage =>
//      incAndPrint
//      if (count > 100)
//        sender ! PingMessage
//      else
//        sender ! StopMessage
//        println("ping stopped")
//        context.stop(self)
//    case _ => 
//      println("cannot understand message")
//  }
//}
//
//abstract class Pong extends Actor { 
//  override def recieve = {
//    case PingMessage => 
//      println("Pong")
//      sender ! PongMessge
//    case StopMessage =>
//      println("Pong stopping")
//      context.stop(self)
//    case _ => 
//      println("cannot understand message")
//   }
//}
//
//abstract class PingPong extends App { 
//  override def recieve = ???
//  
//  val system = ActorSystem("PingPongSystem")
//  val pong = system.actorOf(Props[Pong], name = "Pong actor")
//  val ping = system.actorOf(Props[new Ping(pong)], name = "Ping Actor")
//  
//  ping ! StartMessage
//}
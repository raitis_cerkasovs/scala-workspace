package structural

sealed trait Fel {
  def dinner: Food
}
// Lion, Tiger, Panthe, Cat


final case class Lion() extends Fel {
    def dinner: Food = Anthelope
}

final case class Tiger()  extends Fel {
    def dinner: Food = TigersStuff
}

final case class Panther() extends Fel {
    def dinner: Food = TigersStuff
}

final case class Cat(favFood: String) extends Fel {
    def dinner: Food = Catfood(Anthelope)
}

// polimorfic, patern mutching way - both are ok 

sealed trait Food

final object Anthelope extends Food
final object TigersStuff extends Food
final case class Catfood(food: Food) extends Food

// linked list - recursive datastructure


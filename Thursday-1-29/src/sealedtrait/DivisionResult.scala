package sealedtrait

// Only abstraction of subtypes
sealed trait DivisionResult 

final case class Finite(value: Int) extends DivisionResult
final case object Infinite extends DivisionResult

// divaide(1,2)

object divaide {
  
  def applay(numerator: Int, dominator: Int) = {
    if (dominator == 0) Infinite else Finite(numerator / dominator)
  }
}

object Test extends App {
  divaide.applay(1,0) match {
    case Finite(value) => "It is finite " + value
    case Infinite => "Is infinite"  
   }
  
  
}

// check algebraic
// extend first calss, then with all others - Is-A and Has-a
// is-a pattern, has-a pattern
// structural recursion -  polimorfic or pattern muching(llast for functional programming)

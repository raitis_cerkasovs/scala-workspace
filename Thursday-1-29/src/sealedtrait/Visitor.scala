package sealedtrait

import java.util.Date

// sealed - cannot inherit
// in the file must be all subtypes if sealed

sealed trait Visitor {
  
  def id: String
  def createdAt: Date
  def age: Long = new Date().getTime - createdAt.getTime()
     println("edwe")

}

//case clases gives fields, getters, setters(?)
// final patern

 final case class Anonimous( id: String, createdAt: Date) extends Visitor
  
 final  case class User(id: String,
                     email: String,
                 createdAt: Date = new Date()
                          ) extends Visitor
  
  
 
  object Thing extends App {
   
   def missing(v: Visitor) = {
     v match {
       case User(_,_,_) => "a User"
     }
   }
   
   println("edwe")
 }
 
 // sub zero, not good catch with throwing exceptions
 // try FindBugs
 // pmd source code analyser
package recursive

sealed trait intList 

final case object EndList extends intList
final case class Pair(head: Int, tail: intList) extends intList
  
  // 1 -> 2 -> 3 -> end
  // 1.(2.(3.(end)))

object Thing extends App {
  def sum(list: intList): Int =  { 
         list match {
           case EndList => 0
         }
     // fake return
     // 0     
  }
}

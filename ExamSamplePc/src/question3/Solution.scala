package question3

import scala.math._


object Solution extends App {
   
def estimateB(a: Double, bMin: Double, bMax: Double, err: Double) : Double = {
    val bEst = (bMin+bMax) / 2
    val left = (a+bEst) / a
    val right = a / bEst
    val absDiff = abs(left-right)

    if (absDiff < err)
       bEst
    else {
      if (left < right)
      // bEst is too low
         estimateB(a, bEst, bMax, err)
    else
      // bEst is too high
         estimateB(a, bMin, bEst, err)
}
}
val a = 10.0
val b = estimateB(a, 0, a, .000000001)
println("Estimate is " + (a/b))

}
package question5

object Example extends App {
  
def createFractal(n : Int) : List[String] = {
  if (n == 1)
    // Base case
    List(" |", "/|")
  else {
    // previous version of the factal
    val prev = createFractal(n-1)

    // To produce the top part of the fractal, each of the
    // line of the original fractal should have blank space
    // equal in length to the original line prepended to it
    val top = prev.map( s => (" " * s.length) + s )

    // To produce the bottom part of the fractal, each of the
    // lines of the original fractal should be doubled
    val bottom = prev.map( s => s + s )

    // Now we can just paste the top and bottom parts together
    top ::: bottom
  }
}

def printStringList(list : List[String]) = {
  list.foreach( s => println(s) )
}

print("What size? ")
val n = readLine().toInt

val fractal = createFractal(n)
printStringList(fractal)

println(fractal)

}